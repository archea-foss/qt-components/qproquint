/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "qproquint.h"
#include "proquint/proquint.h"

#include <QRandomGenerator>


using namespace testing;

TEST(BasicValidation, BasicValidation)
{

    QByteArray  cBuff(11, 0);
    quint16 a = 0b1000100100010010;

    uint2quint(cBuff.data(), a, '-');
    EXPECT_EQ(QProQuint::toString(a), QString::fromUtf8(cBuff).trimmed().remove('\x00').right(5));
    EXPECT_EQ(QProQuint::uint16FromString( QProQuint::toString(a) ), a);

    quint32 b = QRandomGenerator::global()->generate();

    uint2quint(cBuff.data(), b, '-');
    // qDebug() << Qt::bin << b << " => " << QProQuint::toString(b) << " original => " << QString::fromUtf8(cBuff).trimmed().remove('\x00');

    EXPECT_EQ(QProQuint::toString(b), QString::fromUtf8(cBuff).trimmed().remove('\x00'));
    EXPECT_EQ(QProQuint::uint32FromString( QProQuint::toString(b) ), b);

}


/*
 * Verifies forward and backward conversion for a set of randomly generated values
 *
 */

TEST(BasicValidation, RandomsVsReference)
{
    QByteArray  cBuff(11, 0);
    // qDebug() << "Run 100000 random values and verify towards original implementation";
    for(int i = 0; i < 1000000; i++)
    {
        quint32 random = QRandomGenerator::global()->generate();

        uint2quint(cBuff.data(), random, '-');
        EXPECT_EQ(QProQuint::toString(random), QString::fromUtf8(cBuff).trimmed().remove('\x00'));

        EXPECT_EQ(QProQuint::uint32FromString( QProQuint::toString(random) ), random);

    }
    // qDebug() << "Random values done.";
}

