/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "qproquint.h"

using namespace testing;

TEST(Uint32StringToValue, BasicTest)
{
    bool bOk = true;
    EXPECT_EQ(QProQuint::uint32FromString("suman-tatib", &bOk), 0xce09d350);
    EXPECT_TRUE(bOk);
}

/*
 * Verifies that not enought quints results in null value.
 */
TEST(Uint32StringToValue, NotEnoughQuints)
{
    bool bOk = true;

    // Not enough quints
    EXPECT_EQ(QProQuint::uint16FromString("tati", &bOk), 0x00);
    EXPECT_FALSE(bOk);

    EXPECT_EQ(QProQuint::uint32FromString("suman-tati", &bOk), 0x00);
    EXPECT_FALSE(bOk);

    EXPECT_EQ(QProQuint::uint32FromString("suma-tati", &bOk), 0x00);
    EXPECT_FALSE(bOk);

    EXPECT_EQ(QProQuint::uint32FromString("suma", &bOk), 0x00);
    EXPECT_FALSE(bOk);
}

/*
 * Verifies that a to long quint-string results in null value.
 */
TEST(Uint32StringToValue, ToLongQuint)
{
    bool bOk = true;

    // To many quints
    EXPECT_EQ(QProQuint::uint32FromString("suman-tatibi", &bOk), 0x00);
    EXPECT_FALSE(bOk);
}
