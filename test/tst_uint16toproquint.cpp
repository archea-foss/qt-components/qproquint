/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "qproquint.h"


using namespace testing;

/*
 * Verifies that a know proquint can be interpreted correctly
 *
 */
TEST(Uint16StringToValue, BasicTest)
{
    bool bOk = true;

    EXPECT_EQ(QProQuint::uint16FromString("tatib", &bOk), 0xd350);
    EXPECT_TRUE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("(null)", &bOk), 0x00);
    EXPECT_FALSE(bOk);

}

/*
 * Verifies that a null-string ends up in null value
 */
TEST(Uint16StringToValue, NullTest)
{
    bool bOk = true;

    EXPECT_EQ(QProQuint::uint16FromString("(null)", &bOk), 0x00);
    EXPECT_FALSE(bOk);

}


/*
 * Verifies that invalid chars invalidates the QProQuint entirely
 *
 */
TEST(Uint16StringToValue, InvalidChar)
{
    bool bOk = true;

    EXPECT_EQ(QProQuint::uint16FromString("xatib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tetib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tayib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tateb", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tatiq", &bOk), 0x00);
    EXPECT_FALSE(bOk);

}

/*
 * Verifies that usage of the consonants and vowels in wrong
 * places results in a null value.
 */
TEST(Uint16StringToValue, WrongTypeOfCharOnWrongPlace)
{
    bool bOk = true;

    // Wrong type of char on the wrong place.
    EXPECT_EQ(QProQuint::uint16FromString("aatib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tmtib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("taaib", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tatjb", &bOk), 0x00);
    EXPECT_FALSE(bOk);
    EXPECT_EQ(QProQuint::uint16FromString("tatiu", &bOk), 0x00);
    EXPECT_FALSE(bOk);

}

/*
 * Verifies that not enought quints results in null value.
 */
TEST(Uint16StringToValue, NotEnoughQuints)
{
    bool bOk = true;

    // Not enough quints
    EXPECT_EQ(QProQuint::uint16FromString("tati", &bOk), 0x00);
    EXPECT_FALSE(bOk);

}

/*
 * Verifies that a to long quint-string results in null value.
 */
TEST(Uint16StringToValue, ToLongQuint)
{
    bool bOk = true;

    // To many quints
    EXPECT_EQ(QProQuint::uint16FromString("tatibi", &bOk), 0x00);
    EXPECT_FALSE(bOk);
}

