##########################################################################
#
# Copyright (c) 2024 Erik Ridderby, ARCHEA.
# All rights reserved.
#
# This source code is licensed under both the
# * BSD-3-Clause license with No Nuclear or Weapons use exception
#   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
#    the root directory of this source tree)
#
# and the
#
# * GNU Affero General Public License
#   (found in the agpl-3.0.txt file in the root directory of this source tree).
#
# You may select, at your option, one of the above-listed licenses.
#
#
##########################################################################

cmake_minimum_required(VERSION 3.27)

project(QProQuint_UnitTests LANGUAGES CXX)

enable_testing()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if ($ENV{GOOGLETEST_DIR})
    get_filename_component(_GTEST_BASE $ENV{GOOGLETEST_DIR} REALPATH)
elseif (EXISTS "") # set by QC
    get_filename_component(_GTEST_BASE "" REALPATH) # set by QC
endif()

if (NOT GTEST_ROOT)
    if (_GTEST_BASE)
        message("Setting GTEST_ROOT to ${_GTEST_BASE}")
        set(GTEST_ROOT ${_GTEST_BASE})
    else()
        message("No GTEST_ROOT specified - using system defaults.")
    endif()
endif()

find_package(GTest REQUIRED)
if (NOT GTest_FOUND)
    message (FATAL_ERROR "No GTest Found")
endif()

# Add Qt components needed to testing
find_package(Qt6 REQUIRED COMPONENTS Core)


# Relate to the component under test:
add_subdirectory("../src" QProQuint)

# Define the Target
add_executable(QProQuint_UnitTests main.cpp
    tst_basic_validation.cpp
    tst_uint16toproquint.cpp
    tst_uint32stringtovalue.cpp
    proquint/proquint.cpp 
    proquint/proquint.h
    )


add_test(NAME QProQuint_UnitTests COMMAND QProQuint_UnitTests)

target_link_libraries(QProQuint_UnitTests   PRIVATE GTest::GTest
                                            PRIVATE Qt6::Core
                                            PRIVATE QProQuint)
if (GMock_FOUND)
    target_link_libraries(QProQuint_UnitTests INTERFACE GTest::GMock)
endif()
