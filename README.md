# QProQuint

Qt and C++ based implementation of proquint conversion. 

ProQuints is a way to express 16 bit values as pronouncable and remeberable combinations 
of consonants and vowels as recomended by Daniel S. Wilkerson in the article ("A Proposal for Proquints:
Identifiers that are Readable, Spellable, and Pronounceable")[http://arXiv.org/html/0901.4016].

In the Preword Daniel writes:

> Identifiers (IDs) are pervasive throughout our modern life. 
> We suggest that these IDs would be easier to manage and remember 
> if they were easily readable, spellable, and pronounceable. As a 
> solution to this problem we propose using PRO-nouncable QUINT-uplets 
> of alternating unambiguous consonants and vowels: proquints.

## ProQuint principles 
The principle are as follows:

Four-bits as a consonant:
```
    0 1 2 3 4 5 6 7 8 9 A B C D E F
    b d f g h j k l m n p r s t v z
```

Two-bits as a vowel:
```
    0 1 2 3
    a i o u
```

Whole 16-bit word, where "con" = consonant, "vo" = vowel.
```

     0 1 2 3 4 5 6 7 8 9 A B C D E F
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |con0   |vo1|con2   |vo3|con4   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |-Hex0--|-Hex1--|-Hex2--|-Hex3--|
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

    
# Copyright and License
Copyright (c) 2024 Erik Ridderby, ARCHEA.
All rights reserved.

This source code is licensed under both the 
* BSD-3-Clause license with No Nuclear or Weapons use exception (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in the root directory of this source tree) and the 
* GNU Affero General Public License (found in the agpl-3.0.txt file in the root directory of this source tree).

You may select, at your option, one of the above-listed licenses.

# Attribution
The idea and design of proquints are work of Daniel S. Wilkerson. The original article can be found at https://arxiv.org/html/0901.4016. 

A copy in AsciiDoc format is included along with the source code in this repository. 
