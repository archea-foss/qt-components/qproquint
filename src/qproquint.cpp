/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#include "qproquint.h"

static const QString UIntCons="bdfghjklmnprstvz";
static const QString UIntVos="aiou";

union quint16bit
{
        quint16     word;
        struct
        {

                unsigned int    con3:4;
                unsigned int    vo2:2;
                unsigned int    con2:4;
                unsigned int    vo1:2;
                unsigned int    con1:4;
        } bits;
};

QString QProQuint::toString(quint16 i)
{
    // An quint16 has 5 parts:
    // con, vo, con, vo, co

    QString result;

    quint16bit  convert;
    convert.word = i;

    result += UIntCons[convert.bits.con1];
    result += UIntVos[convert.bits.vo1];
    result += UIntCons[convert.bits.con2];
    result += UIntVos[convert.bits.vo2];
    result += UIntCons[convert.bits.con3];


    return result;
}

QString QProQuint::toString(quint32 i, QChar separator)
{
    quint16 msb = static_cast<quint16>(i >> 16);
    quint16 lsb = static_cast<quint16>(i);

    QString result;

    result += toString(msb);
    if (!separator.isNull())
        result += separator;

    result += toString(lsb);

    return result;

}

quint16 QProQuint::uint16FromString(const QString &quint, bool *bOk)
{
    quint16bit  convert;

    if (bOk != nullptr)
        *bOk = true;

    if (quint.length() != 5)
    {
        if (bOk != nullptr)
            *bOk = false;

        return 0;
    }

    quint16 partValue = consonantToValue(quint[0], 0x100);
    if (partValue < 0x100)
        convert.bits.con1 = partValue;
    else
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    partValue = vowelToValue(quint[1], 0x100);
    if (partValue < 0x100)
        convert.bits.vo1 = partValue;
    else
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    partValue = consonantToValue(quint[2], 0x100);
    if (partValue < 0x100)
        convert.bits.con2 = partValue;
    else
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    partValue = vowelToValue(quint[3], 0x100);
    if (partValue < 0x100)
        convert.bits.vo2 = partValue;
    else
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    partValue = consonantToValue(quint[4], 0x100);
    if (partValue < 0x100)
        convert.bits.con3 = partValue;
    else
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    return convert.word;

}

quint32 QProQuint::uint32FromString(const QString &quint, bool *bOk)
{
    quint32     result = 0x00;

    bool bOwnOk = true;

    // We accept only 10 or 11 chars (10 wihthout separator, 11 with separator).

    if ( quint.length() < 10 || 11 < quint.length())
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    result += uint16FromString(quint.left(5), &bOwnOk);
    if (bOwnOk != true)
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }


    result = result << 16;

    result += uint16FromString(quint.right(5), &bOwnOk);
    if (bOwnOk != true)
    {
        if (bOk != nullptr)
            *bOk = false;
        return 0;
    }

    return result;

}

quint16 QProQuint::consonantToValue(QChar ch, quint16 defaultValue)
{
    switch(ch.toLatin1())
    {
        case 'b':
            return 0x00;

        case 'd':
            return 0x01;

        case 'f':
            return 0x02;

        case 'g':
            return 0x03;

        case 'h':
            return 0x04;

        case 'j':
            return 0x05;

        case 'k':
            return 0x06;

        case 'l':
            return 0x07;

        case 'm':
            return 0x08;

        case 'n':
            return 0x09;

        case 'p':
            return 0x0a;

        case 'r':
            return 0x0b;

        case 's':
            return 0x0c;

        case 't':
            return 0x0d;

        case 'v':
            return 0x0e;

        case 'z':
            return 0x0f;

        default:
            return defaultValue;

    }

    return defaultValue;
}

quint16 QProQuint::vowelToValue(QChar ch, quint16 defaultValue)
{
    switch(ch.toLatin1())
    {

        case 'a':
            return 0x00;

        case 'i':
            return 0x01;

        case 'o':
            return 0x02;

        case 'u':
            return 0x03;

        default:
            return defaultValue;

    }

    return defaultValue;
}

