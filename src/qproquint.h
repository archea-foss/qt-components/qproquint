/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#ifndef QPROQUINT_H
#define QPROQUINT_H

#include <QString>
#include <QtTypes>

#include "QProQuint_global.h"
/**
 * @brief The QProQuint class
 *  Convert between proquint, hex, and decimal strings.
 *  Please see the article on proquints:
 *  http://arXiv.org/html/0901.4016
 *
 *  Rewrite of Daniel S. Wilkerson proquint C code found at:
 *  https://github.com/dsw/proquint
 *
 *
 *  Four-bits as a consonant:
 *  0 1 2 3 4 5 6 7 8 9 A B C D E F
 *  b d f g h j k l m n p r s t v z
 *
 *  Two-bits as a vowel:
 *  0 1 2 3
 *  a i o u
 *
 *  Whole 16-bit word, where "con" = consonant, "vo" = vowel.
 *  0 1 2 3 4 5 6 7 8 9 A B C D E F
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |con0   |vo1|con2   |vo3|con4   |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |-Hex0--|-Hex1--|-Hex2--|-Hex3--|
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class QPROQUINT_EXPORT QProQuint
{
    public:

        /**
         * @brief toString converts a quint16 to a 5 letter quint
         * @param i The value to be converted.
         * @return Quint in a QString
         */
        static QString toString(quint16 i);

        /**
         * @brief toString converts a quint32 to two 5 letter quint with separator
         * @param i The value to be converted
         * @param separator Separator between the two quints or Null for no separator
         * @return Q duble quint as a QString
         */
        static QString toString(quint32 i, QChar separator = '-');

        /* Map a quint to a uint, skipping non-coding characters. */
        /**
         * @brief uint16FromString Parses a quint string into a quint14
         * @param quint String to parse with 1 quint (1x5 chars).
         * @param bOk Pointer to Boolean. If supplied it will indicate if the operation was successfull (true) or not (false)
         * @return A interpreted quint16 or 0x00 on failure.
         */
        static quint16 uint16FromString(const QString& quint, bool* bOk = nullptr);

        /**
         * @brief uint32FromString Parses a quint string into a quint32
         * @param quint String to parse with 2 quintes (2x5 chars + separator).
         * @param bOk Pointer to Boolean. If supplied it will indicate if the operation was successfull (true) or not (false)
         * @return A interpreted quint32 or 0x00 on failure.
         */
        static quint32 uint32FromString(const QString& quint, bool* bOk = nullptr);

    private:

        static quint16 consonantToValue(QChar ch, quint16 defaultValue = 0x100);
        static quint16 vowelToValue(QChar ch, quint16 defaultValue = 0x100);


};

#endif // QPROQUINT_H
