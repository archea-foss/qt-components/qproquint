/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */

#ifndef QPROQUINT_GLOBAL_H
#define QPROQUINT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QPROQUINT_LIBRARY)
#  define QPROQUINT_EXPORT Q_DECL_EXPORT
#else
#  define QPROQUINT_EXPORT Q_DECL_IMPORT
#endif

#endif // QPROQUINT_GLOBAL_H
